package com.example.my_practice_1.retrofit

import android.util.Log.d
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface RetrofitApi {
    @GET("{path}")
    fun getRequest(@Path("path") path: String?): Call<String>?
}

object RetrofitClient{
    lateinit var apiData : String

    var retrofit = Retrofit.Builder()
        .baseUrl("https://reqres.in/api/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()
    var service: RetrofitApi = retrofit.create(RetrofitApi::class.java)

    fun getRequest(path: String?, customCallback: CustomCallback){
        val Call : Call<String> = service.getRequest(path)!!
        Call.enqueue(callback(customCallback))
    }
    private fun callback(customCallback : CustomCallback) : Callback<String>{
        return object : Callback<String>{
            override fun onResponse(call: Call<String>, response: Response<String>) {
                apiData = response.body().toString()
                d("myData", "${response.body()}")
                customCallback.OnSuccess(response.body().toString())
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                TODO("Not yet implemented")
            }

        }

    }

}