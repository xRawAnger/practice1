package com.example.my_practice_1.adapters

import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.my_practice_1.databinding.RecyclerItemBinding
import com.example.my_practice_1.model.ItemModel


class RecyclerViewAdapter(
    private var items: ItemModel?,
    private val onRecyclerViewListItemClick: (position: Int) -> Unit
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder(
        val binding: RecyclerItemBinding,
        onRecyclerViewListItemClick: (position: Int) -> Unit
    ) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            onRecyclerViewListItemClick(position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RecyclerItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ViewHolder(binding,onRecyclerViewListItemClick)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var image = Glide.with(holder.binding.imageView.context).load(items!!.data[position].avatar)
            .into(holder.binding.imageView)
        holder.binding.firstName.text = items!!.data[position].firstName
        holder.binding.lastName.text = items!!.data[position].lastName
    }

    override fun getItemCount(): Int {
        return items!!.data.size
    }


}