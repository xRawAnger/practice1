package com.example.my_practice_1.views

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.my_practice_1.retrofit.CustomCallback
import com.example.my_practice_1.adapters.RecyclerViewAdapter
import com.example.my_practice_1.databinding.ActivityMainBinding
import com.example.my_practice_1.model.ItemModel
import com.example.my_practice_1.retrofit.RetrofitClient
import com.google.gson.Gson


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding



    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        val binding:ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init(binding)
        getUsers(binding)
    }

    private fun init(binding:ActivityMainBinding){
            binding.recyclerView.layoutManager = LinearLayoutManager(this)
    }
    private fun getUsers(binding:ActivityMainBinding){
        RetrofitClient.getRequest(
            path = "users",
            object : CustomCallback {
                override fun OnSuccess(result: String) {
                    val model: ItemModel? = Gson().fromJson(result, ItemModel::class.java)
                    binding.recyclerView.adapter = RecyclerViewAdapter(model){
                            position -> onRecyclerViewListItemClick(position)
                    }
                }
            }
        )
    }

    private fun addFragment(container: Int,fragment: Fragment,tag : String, hasBackStack: Boolean){
        val transaction = supportFragmentManager.beginTransaction()
        if(hasBackStack){
            transaction.replace(container,fragment,tag)
            transaction.addToBackStack(tag)
        }else
            transaction.replace(container,fragment)
        transaction.commit()
    }
    private fun onRecyclerViewListItemClick(position: Int) {
        val intent = Intent(this, RecyclerItemDetailActivity::class.java)
        intent.putExtra("POSITION",position.toInt())
        val bundle = Bundle()
        bundle.putSerializable("serializable", ItemModel())
        intent.putExtras(bundle)
        startActivity(intent)
    }

}