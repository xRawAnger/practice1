package com.example.my_practice_1.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.my_practice_1.R
import com.example.my_practice_1.databinding.ActivityRecyclerItemDetailBinding
import com.example.my_practice_1.fragments.AppbarFragment
import com.example.my_practice_1.model.ItemModel
import com.example.my_practice_1.retrofit.CustomCallback
import com.example.my_practice_1.retrofit.RetrofitClient
import com.google.gson.Gson

class RecyclerItemDetailActivity : AppCompatActivity(){

    private var userInfo: ItemModel? = null
//    private lateinit var binding: ActivityRecyclerItemDetailBinding
    private var position:Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityRecyclerItemDetailBinding = ActivityRecyclerItemDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
        getUserData(binding)
        appBar(binding)
        hideAppBar()
    }

    private fun hideAppBar(){
        supportActionBar?.hide()
    }




    private fun init(){
        val intent:Intent = intent
        position = intent.getIntExtra("POSITION",1)
        d("position","$position")
        userInfo = intent.getSerializableExtra("serialzable") as ItemModel?
    }







    private fun appBar(binding: ActivityRecyclerItemDetailBinding){
        addFragment(R.id.appBarSlot,AppbarFragment(),"appBar",false)
    }






    private fun addFragment(container: Int, fragment: Fragment, tag : String, hasBackStack: Boolean){
        val transaction = supportFragmentManager.beginTransaction()
        if(hasBackStack){
            transaction.replace(container,fragment,tag)
            transaction.addToBackStack(tag)
        }else
            transaction.replace(container,fragment)
        transaction.commit()
    }





    private fun getUserData(binding: ActivityRecyclerItemDetailBinding){
        RetrofitClient.getRequest(
            path = "users",
            object : CustomCallback {
                override fun OnSuccess(result: String) {
                    val model: ItemModel? = Gson().fromJson(result, ItemModel::class.java)
                    Glide.with(binding.profileImage.context).load(model!!.data[position].avatar)
                        .into(binding.profileImage)
                    binding.profileName.text = model.data[position].firstName
                    binding.profileLastName.text = model.data[position].lastName
                    binding.profileEmail.text = model.data[position].email
                }
            }
        )
    }
}