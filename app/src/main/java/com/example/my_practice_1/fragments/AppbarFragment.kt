package com.example.my_practice_1.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.my_practice_1.R
import com.example.my_practice_1.databinding.FragmentAppbarBinding




class AppbarFragment : Fragment() {

//    private lateinit var binding: FragmentAppbarBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding: FragmentAppbarBinding = FragmentAppbarBinding.inflate(layoutInflater)
        binding.appBar.inflateMenu(R.menu.simple_menu)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentAppbarBinding = FragmentAppbarBinding.inflate(layoutInflater)
        init()
        return binding.root
    }
    private fun init(){

    }

    private fun clearToolbarMenu(binding: FragmentAppbarBinding) {
        binding.appBar.menu.clear()
    }


}